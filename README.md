# Genie Tutorial

## Hello World

### Code

```genie
[indent=4]

init
    print("Hello, World!")
```

### Description

`init` is a special keyword which marks the entry point for the program.

`print` is a function that takes in a string and prints it to the standard output.

`[indent=4]` sets indentation to 4 spaces. You can choose to omit it but without it, you will have to use tabs to indent. You can replace 4 with any number of spaces you want to use for indentation.

**Note**: In Genie, just like Python, indentation is very important. Make sure your code is indented properly.

### Compiling and Running

Genie code is compiled using the [Vala compiler](https://gitlab.gnome.org/GNOME/vala). So, make sure Vala compiler is installed and then:

1. Save the hello world code as `hello-world.gs`.  
   NOTE: the `.gs` extension

2. Run the command `valac hello-world.gs`.  
   This will compile the code into an executable named `hello-world`.

3. You can run the executable by running `./hello-world`.

## Basics

### init block

The `init` block is a special function which marks the entry point for a program. It is analogous to the `main` function in C/C++.

It recieves a parameter named `args` of `array of string` type. `args` contains arguments provided on the command-line when the program was launched. This also includes program name.

By default, it does not return anything.

#### Example Code

```genie
init
    var program_name = args[0]
    var arguments = args[1:]
    print(@"$(program_name) invoked with $(arguments.length) arguments")
```

#### Return Value

Just like many other languages, Genie allowes the entry point (`init` block) to return an integer to indicate if the program succeeded or failed. You can declare the init block to return an integer by appending `: int` after the `init` keyword. For example, we can modify the above example code to be

```genie
init: int
    var program_name = args[0]
    var arguments = args[1:]
    print(@"$(program_name) invoked with $(arguments.length) arguments")
    return 0
```

If return value is 0, it means the program succeeded. Any other value indicates failure.

#### As a Regular Function

We can define the `init` block as a regular function too but the function name must be `main`.

```genie
def main(args: array of string): int
    var program_name = args[0]
    var arguments = args[1:]
    print(@"$(program_name) invoked with $(arguments.length) arguments")
    return 0
```

In this form, we can also omit one or both of return type and `args` parameter.

```genie
def main()
    // There is no `args` parameter. So, we can't use it anymore
    print("Hello")
```
